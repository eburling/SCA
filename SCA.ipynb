{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Single-cell analysis hands-on session\n",
    "Erik Burlingame\n",
    "\n",
    "Chang Lab\n",
    "\n",
    "OHSU\n",
    "\n",
    "2020-12-2\n",
    "\n",
    "## Contents\n",
    "### 1. Overview\n",
    "### 2. Normalizing multiplex intensity measurements with RESTORE (in brief)\n",
    "### 3. Fast single-cell phenotyping\n",
    "### 4. Visualization\n",
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Overview\n",
    "- **Dataset:** 180 cores from pan-subtype breast cancer TMAs stained by CyCIF panel for tumor, immune, and stromal cell populations (credit to Jenny Eng and Koei Chin).\n",
    "- **Problem 1:** compilation and comparison of mean intensity features between TMAs and cores confounded by batch effects.\n",
    "- **Solution 1:** Intensity normalization using mutually exclusive information encoded in CyCIF panel.\n",
    "- **Problem 2:** Defining phenotypes among millions of cells in a high-dimensional, continuous space is non-trivial and computationally expensive.\n",
    "- **Solution 2:** Phenotyping using a robust, non-parametric algorithm (PhenoGraph) which has been accelerated through GPU implementation.\n",
    "- **For another time:** spatial analysis.\n",
    "![](img/overview.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Let's import the libraries we'll be using then take a look at these data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cudf # RAPIDS analog of pandas\n",
    "import cuml # RAPIDS analog of sklearn\n",
    "import RESTORE # normalization\n",
    "import pandas as pd # dataframe operations\n",
    "import seaborn as sns # clustergram viz\n",
    "import grapheno # GPU-accelerated phenograph\n",
    "import numpy as np # array operations\n",
    "import holoviews as hv # viz\n",
    "import datashader as ds # viz for large data\n",
    "from viz import get_dendrogram # for hv dendrogram\n",
    "from sklearn.preprocessing import minmax_scale # data scaling\n",
    "from holoviews.operation.datashader import datashade # viz for large data\n",
    "from colorcet import fire, glasbey_hv # perceptually accurate colormaps\n",
    "hv.extension('bokeh') # specify which library for plotting, e.g. 'bokeh' or 'matplotlib'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = cudf.read_csv('df.csv')\n",
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_marker_mask = df.columns.str.contains('Nuclei|Ring') * ~df.columns.str.contains('Enorm')\n",
    "raw_marker_cols = df.columns[raw_marker_mask]\n",
    "print('CyCIFmarker_compartment:')\n",
    "raw_marker_cols.to_list()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- 1.25 million cells in total.\n",
    "- 35 markers in CyCIF panel.\n",
    "- Now let's see how the data break down across BC subtype."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "core_df = df[['source','scene','clinical_subtype']].drop_duplicates().groupby(['source','clinical_subtype']).size().reset_index().rename(columns={0:'core count'})\n",
    "core_bars = hv.Bars(core_df,\n",
    "        kdims=['source','clinical_subtype'],\n",
    "        vdims='core count').opts(width=520,\n",
    "                                 height=200,\n",
    "                                 toolbar=None,\n",
    "                                 xrotation=90,\n",
    "                                 tools=['hover'],\n",
    "                                 cmap={'TN':'#b3b3b3','HR+HER2-':'#e5c494','HR+HER2+':'#a6d854','HR-HER2+':'#ffd92f'},\n",
    "                                 stacked=False,\n",
    "                                 show_legend=False,\n",
    "                                 yticks=5,\n",
    "                                 xaxis='bare')\n",
    "\n",
    "cell_df = df[['source','scene','clinical_subtype']].groupby(['source','clinical_subtype']).size().reset_index().rename(columns={0:'cell count'})                                                                                          \n",
    "cell_bars = hv.Bars(cell_df,\n",
    "                    kdims=['source','clinical_subtype'],\n",
    "                    vdims='cell count').opts(width=520,\n",
    "                                             height=300,\n",
    "                                             toolbar=None,\n",
    "                                             tools=['hover'],\n",
    "                                             xrotation=90,\n",
    "                                             show_legend=False,\n",
    "                                             cmap={'TN':'#b3b3b3','HR+HER2-':'#e5c494','HR+HER2+':'#a6d854','HR-HER2+':'#ffd92f'},\n",
    "                                             stacked=False,\n",
    "                                             yformatter='%.0e',\n",
    "                                             yticks=5)\n",
    "\n",
    "(core_bars + cell_bars).opts(toolbar=None).cols(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Core diameter varies substantially between TMAs.\n",
    "- Data are dominated by cells from TN tissues.\n",
    "- Now on to normalization..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. RESTORE: robust intensity normalization method for multiplexed imaging ([PAPER](https://www.nature.com/articles/s42003-020-0828-1), [CODE](https://gitlab.com/Chang_Lab/cycif_int_norm))\n",
    "- RESTORE uses orthogonal staining patterns to set background staining thresholds for intensity normalization. \n",
    "- **Key assumption:** cell types defined by reference markers can be safely assumed to have background levels for other markers. \n",
    "- **Insight:** intensity for the reference markers (e.g. CK19) can be normalized by dividing by the inferred background level from the negative marker (e.g. CD31).\n",
    "- **Gut check:** This assumption is inaccurate for low signal levels but becomes increasingly accurate for high signals based on our simulation study.\n",
    "\n",
    "![](https://media.springernature.com/full/springer-static/image/art%3A10.1038%2Fs42003-020-0828-1/MediaObjects/42003_2020_828_Fig7_HTML.png?as=webp)\n",
    "\n",
    "<!-- ![](https://media.springernature.com/lw685/springer-static/image/art%3A10.1038%2Fs42003-020-0828-1/MediaObjects/42003_2020_828_Fig6_HTML.png?as=webp) -->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "QUERY_MARKER = 'CK19_Ring'\n",
    "QUERY_SOURCE = 'Her2B-K154'\n",
    "QUERY_SCENE = 2\n",
    "\n",
    "tissue_mask = (df.source==QUERY_SOURCE) & (df.scene==QUERY_SCENE)\n",
    "query_df = df.loc[tissue_mask][raw_marker_cols]\n",
    "query_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Choosing mutually-exclusive marker pairs can be motivated by:\n",
    "    - biology, e.g. tumor vs. immune markers. \n",
    "    - the data itself:\n",
    "    > Since the singular values can be interpreted as the semi-axis of an ellipse in 2D, we measure the mutually exclusiveness of two marker expressions as a ratio (r=σ2/σ1). If two markers are highly correlated with each other, we will get an elongated ellipse (i.e., r is close to zero). On the other hand, if they are mutually exclusive, you will get a more circular shape (i.e., close to one).\n",
    "    - or both."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r_vals = []\n",
    "figs = []\n",
    "for marker in query_df:\n",
    "    if marker != QUERY_MARKER:\n",
    "        \n",
    "        X = query_df[[QUERY_MARKER, marker]]\n",
    "        svd = cuml.TruncatedSVD(n_components=2)\n",
    "        svd.fit(X)\n",
    "        r = svd.singular_values_[1] / svd.singular_values_[0]\n",
    "        r_vals.append((marker, r))\n",
    "        \n",
    "        fig = datashade(hv.Scatter(X, QUERY_MARKER, [marker]),\n",
    "                        cmap=fire,\n",
    "                        normalization='log')\n",
    "        figs.append(fig)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r_box = hv.BoxWhisker(r_vals,\n",
    "                      'neg_marker',\n",
    "                      'r_val').opts(title=QUERY_MARKER,\n",
    "                                    xrotation=45,\n",
    "                                    width=800,\n",
    "                                    toolbar=None).sort('r_val')\n",
    "r_box"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# bi_scatters = hv.Layout(figs).cols(4)\n",
    "# bi_scatters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BG_MARKER = 'CD68_Ring'\n",
    "X = RESTORE.process_data(query_df[[QUERY_MARKER, BG_MARKER]], \n",
    "                         QUERY_MARKER, BG_MARKER)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "norm_factor, clusters = RESTORE.get_ssc_thresh(X.to_pandas())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(hv.Scatter(clusters[0]) * hv.Scatter(clusters[1]) * hv.VLine(norm_factor).opts(color='black')).opts(xlabel=QUERY_MARKER,\n",
    "                                                                                                     ylabel=BG_MARKER)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the RESTORE paper:\n",
    ">Normalization factors are computed for each pair of reference and mutually-exclusive markers, and the median of these factors is used to normalize each raw single-cell mean intensity vector for each CyCIF marker and each TMA core. Raw intensities were normalized using the equation:\n",
    ">$$\\hat{\\mathbf{x}}_{i,j} = \\frac{\\mathbf{x}_{i,j}-min(\\mathbf{x}_{i,j})}{\\phi_{i,j}-min(\\mathbf{x}_{i,j})}$$\n",
    ">where $\\hat{\\mathbf{x}}_{i,j}$ and $\\mathbf{x}_{i,j}$ are the normalized and raw single-cell mean intensity vectors for CyCIF marker $i$ for all cells in tissue core $j$, respectively, and $\\phi_{i,j}$ is the corresponding normalization factor determined as described above. Therefore, cells with a normalized intensity greater than 1 are considered to be above the background intensity level.\n",
    "\n",
    "- RESTORE normalized data in the dataframe are prefaced with 'Enorm_scene'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "norm_marker_mask = df.columns.str.contains('Nuclei|Ring') * df.columns.str.contains('Enorm')\n",
    "norm_marker_cols = df.columns[norm_marker_mask]\n",
    "df[norm_marker_cols]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Fast single-cell phenotyping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PhenoGraph algorithm consists of two primary steps: \n",
    "\n",
    "1) defining a $k$-nearest neighbor graph over all cells that is then refined by computing the Jaccard similarity measure over graph edges.\n",
    "\n",
    "2) partitioning the graph into discrete cell phenotypes through optimization of partition modularity.\n",
    "\n",
    "- In the [official version of PhenoGraph](https://github.com/dpeerlab/PhenoGraph), these steps are implemented using Python and C++ libraries that execute on CPU.\n",
    "- PhenoGraph execution time increases exponentially with increasing dataset size, taking ~3 hours to process a synthetic 1 million cell-by-10 feature dataset.\n",
    "- Most MTI datasets measure tens of features, but CPU-based PhenoGraph was unable to fully process the 1 million cell-by-30- and 50-feature synthetic datasets in the 8 hours allotted for the experiment. \n",
    "- We see such computational bottlenecks--which would be even further constricted when compiling multiple MTI or cytometry datasets--as a major obstacle to current studies and future meta-studies of high-dimensional MTI datasets, where rapid iteration will be essential to the validation of cross-platform data integration techniques.\n",
    "- We employed CuPy and RAPIDS, to accelerate each step of the PhenoGraph algorithm and enable distributed computing over multiple GPUs. \n",
    "![](img/benchmark.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- We first pre-process data as suggested for PhenoGraph and t-SNE, then call our GPU-accelerated implementation of PhenoGraph.\n",
    "- Dataframe preprocessing in cudf is not yet mature, so we use pandas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# extract expression cols\n",
    "X = df[norm_marker_cols].to_pandas()# - 1\n",
    "\n",
    "# remove outliers\n",
    "X = X.clip(upper=X.quantile(q=.999),axis=1)\n",
    "\n",
    "# deskew\n",
    "X = (X/5).apply(np.arcsinh)\n",
    "\n",
    "# scale\n",
    "X = X.apply(minmax_scale)\n",
    "\n",
    "# identify clusters\n",
    "clusters, _, _ = grapheno.cluster(cudf.from_pandas(X),\n",
    "                                  n_neighbors=40,\n",
    "                                  community='louvain')\n",
    "\n",
    "# add back to df\n",
    "df['clusters'] = clusters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Vizualization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- We can visualize the signature of each derived cluster using a heatmap.\n",
    "- We will borrow the clustermap function from seaborn to build a heatmap, then use its structure to build a more complex figure with holoviews."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = sns.clustermap(df[norm_marker_cols.to_list()+['clusters']].to_pandas().groupby('clusters').mean().drop(-1.0,errors='ignore'),\n",
    "                   z_score=1,\n",
    "                   cmap='Spectral_r',\n",
    "                   method='ward',\n",
    "                   center=0,\n",
    "                   figsize=(12,20))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%opts Layout [shared_axes=False] \n",
    "\n",
    "community = 'clusters'\n",
    "\n",
    "# Define main heatmap\n",
    "height = 800\n",
    "\n",
    "melted = pd.melt(g.data2d.reset_index(),\n",
    "                 id_vars='clusters',\n",
    "                 var_name='marker',\n",
    "                 value_name='expression')\n",
    "\n",
    "melted.clusters = melted.clusters.astype('str')\n",
    "\n",
    "hm = hv.HeatMap(melted,kdims=['marker','clusters']).opts(width=700,\n",
    "                                                          height=height,\n",
    "                                                          border=0,\n",
    "                                                          tools=['hover'],\n",
    "                                                          colorbar=True,\n",
    "                                                          colorbar_position='bottom',\n",
    "                                                          cmap='Spectral_r',\n",
    "                                                          symmetric=True)\n",
    "\n",
    "hm_fig = get_dendrogram(hm, ['clusters','marker'], 'expression')\n",
    "\n",
    "# Define subtype/cluster cell counts bars\n",
    "community_sort = melted[community].drop_duplicates()\n",
    "counts = df.to_pandas().groupby([community,'clinical_subtype']).size().drop(-1.0,errors='ignore')   # Count cells in each community\n",
    "counts = counts.unstack(fill_value=0).stack().to_frame().reset_index()                  # Make sure to include zeros\n",
    "counts[community] = counts[community].astype('category')                                # Make categorical for sorting\n",
    "counts[community].cat.set_categories(community_sort.astype('int64'),inplace=True)                       # Set category sorting\n",
    "\n",
    "subtype_sort = reversed(['TN','HR+HER2-','HR-HER2+','HR+HER2+'])\n",
    "counts.clinical_subtype = counts.clinical_subtype.astype('category')      # Make categorical for sorting\n",
    "counts.clinical_subtype.cat.set_categories(subtype_sort,inplace=True)  # Set category sorting\n",
    "\n",
    "counts = counts.sort_values([community,'clinical_subtype']).rename(columns={0:'cell count'}).iloc[::-1] # Sort and rename\n",
    "counts['cell count'] = counts['cell count'].div(1e4)                 # Rescale for axis ticks                 \n",
    "\n",
    "def hook(plot, element):\n",
    "    plot.handles['plot'].min_border_right=5\n",
    "\n",
    "bars = hv.Bars(counts,[community,'clinical_subtype'],'cell count').opts(invert_axes=True,\n",
    "                                                                          invert_xaxis=True,\n",
    "#                                                                           invert_yaxis=True,\n",
    "                                                                          show_frame=False,\n",
    "                                                                          width=170,\n",
    "                                                                          height=height,\n",
    "                                                                          yaxis=None,\n",
    "                                                                          ylabel=f'cells in cluster (×10\\u2074)',\n",
    "                                                                          border=0,\n",
    "                                                                          show_legend=False,\n",
    "                                                                          tools=['hover'],\n",
    "                                                                          stacked=True,\n",
    "                                                                          cmap='Set2_r',\n",
    "                                                                          hooks=[hook])\n",
    "\n",
    "# Define subtype/cluster proportion scatter plot\n",
    "dot = df.to_pandas().groupby(['clinical_subtype',community]).size().unstack(fill_value=0).stack().reset_index().rename(columns={0:'counts'})\n",
    "dot = dot[dot[community]!=-1]\n",
    "dot[community] = dot[community].astype('category')\n",
    "dot[community].cat.set_categories(community_sort[::-1].astype('int64'),inplace=True)\n",
    "\n",
    "sorted_dot = []\n",
    "for cs in ['HR+HER2+', 'HR-HER2+', 'HR+HER2-', 'TN']:\n",
    "    total = dot[dot.clinical_subtype==cs].counts.sum()\n",
    "    dot.loc[dot.clinical_subtype==cs,'counts'] = (dot.loc[dot.clinical_subtype==cs]['counts'] / total) * 100\n",
    "    dot.loc[dot.clinical_subtype==cs] = dot.loc[dot.clinical_subtype==cs].iloc[community_sort[::-1]]\n",
    "    sorted_dot.append(dot.loc[dot.clinical_subtype==cs].sort_values(community))\n",
    "\n",
    "dot = pd.concat(sorted_dot).rename(columns={'counts':'% of subtype'}).iloc[::-1]\n",
    "dot[community] = dot[community].astype(str)\n",
    "\n",
    "def hook(plot, element):\n",
    "    plot.handles['plot'].min_border_right=5\n",
    "    plot.handles['plot'].min_border_right=5\n",
    "    \n",
    "scat = hv.Scatter(dot).opts(height=height,\n",
    "                            size=hv.dim('% of subtype'),\n",
    "                            tools=['hover'],\n",
    "                            border=0,\n",
    "                            show_frame=False,\n",
    "                            alpha=0.5,\n",
    "                            yaxis=None,\n",
    "                            xlabel='clinical subtype',\n",
    "                            xrotation=90,\n",
    "                            width=120,\n",
    "                            invert_yaxis=True,\n",
    "                            color=hv.dim('clinical_subtype'),\n",
    "                            cmap='Set2_r',\n",
    "                            show_legend=False,\n",
    "                            hooks=[hook])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bars + scat + hm_fig.redim.range(expression=(-2, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- We can now use the GPU-accelerated implementations of t-SNE and UMAP in combination with datashader for full dataset embedding and visualization.\n",
    "- You will have to experiment with the parameters for t-SNE and UMAP to derive useful embeddings.\n",
    "- There were some changes made to the t-SNE implementation related to perplexity between rapids-0.15 and rapids-0.16 that have significantly affected embeddings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "PERPLEXITY = 60\n",
    "N_NEIGHBORS = 3 * PERPLEXITY\n",
    "tsne = cuml.TSNE(random_state=42, perplexity=PERPLEXITY, n_neighbors=N_NEIGHBORS)\n",
    "tsne_coords = tsne.fit_transform(cudf.from_pandas(X)).rename(columns={0:'TSNE1',1:'TSNE2'})\n",
    "\n",
    "tsne_coords['clusters'] = df.clusters\n",
    "\n",
    "color_key = ['#%02x%02x%02x' % tuple(int(255*j) for j in glasbey_hv[i]) for i in range(len(df.clusters.unique()))]\n",
    "\n",
    "tsne_embed = datashade(hv.Scatter(tsne_coords),\n",
    "                       aggregator=ds.count_cat('clusters'),\n",
    "                       color_key=color_key).opts(aspect='equal',\n",
    "                                                 padding=0.1,\n",
    "                                                 xaxis=None,\n",
    "                                                 yaxis=None,\n",
    "                                                 width=700,\n",
    "                                                 height=700)\n",
    "tsne_embed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "umap = cuml.UMAP()\n",
    "umap_coords = umap.fit_transform(cudf.from_pandas(X)).rename(columns={0:'UMAP1',1:'UMAP2'})\n",
    "\n",
    "umap_coords['clusters'] = clusters\n",
    "\n",
    "umap_embed = datashade(hv.Scatter(umap_coords),\n",
    "                       aggregator=ds.count_cat('clusters'),\n",
    "                       color_key=color_key).opts(padding=0.1,\n",
    "                                                 xaxis=None,\n",
    "                                                 yaxis=None,\n",
    "                                                 width=700,\n",
    "                                                 height=700)\n",
    "umap_embed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Epilogue\n",
    "- Single-cell data normalization and phenotyping is hard.\n",
    "    - Even validated and community-accepted methods can take hours or days to execute.\n",
    "- RESTORE normalization may work for you if your data fit some key assumptions and you choose mutually-exclusive marker pairs carefully.\n",
    "    - A GPU-accelerated version of RESTORE is coming soon.\n",
    "- CuPy and RAPIDS offer GPU-accelerated data science primitives.\n",
    "    - Use them to boost your existing work, or invent new and blazing-fast algorithms altogether."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:rapids-0.16]",
   "language": "python",
   "name": "conda-env-rapids-0.16-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
